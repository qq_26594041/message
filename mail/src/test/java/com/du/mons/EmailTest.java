package com.du.mons;

import com.du.mons.service.EmailService;
import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 杜瑞龙
 * @date 2023/9/9 12:03
 */
@SpringBootTest
public class EmailTest {

    @Resource
    EmailService emailService;
    @Test
    public  void simSend(){
        emailService.sendEmail("duruilongTest@163.com","demo","test");
    }
}
