package com.du.kafka;

import ch.qos.logback.core.net.SyslogOutputStream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@SpringBootTest
@Slf4j
class KafkaApplicationTests {

    @Autowired
     KafkaTemplate<String,Object> kafkaTemplate;

    /**
     * 同步发送
     */
    @Test
    void sync() throws ExecutionException, InterruptedException {
        CompletableFuture<SendResult<String, Object>> send = kafkaTemplate.send("du", "001", "de");

        SendResult<String, Object> stringObjectSendResult = send.get();
        RecordMetadata recordMetadata = stringObjectSendResult.getRecordMetadata();
        long offset = recordMetadata.offset();
        int partition = recordMetadata.partition();

        ProducerRecord<String, Object> producerRecord = stringObjectSendResult.getProducerRecord();
        String key = producerRecord.key();
        Object value = producerRecord.value();

    }

    /**
     * 异步发送
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @Test
    void async() throws ExecutionException, InterruptedException {
        CompletableFuture<SendResult<String, Object>> send = kafkaTemplate.send("du", "001", "de");


    }
}
