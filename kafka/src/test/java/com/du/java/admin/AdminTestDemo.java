package com.du.java.admin;

import org.apache.kafka.clients.admin.*;
import org.apache.kafka.common.config.ConfigResource;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.*;

public class AdminTestDemo {

    static AdminClient adminClient;
    static String TOPIC_NAME = "du";

    /**
     * 创建admin-client
     */
    @BeforeAll
    public static void createAdminClient(){
        Properties properties = new Properties();
        properties.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "82.157.62.33:9092");
        adminClient = AdminClient.create(properties);
    }

    /**
     * 创建Topic实例
     */
    @Test
    public void createTopic() {
        short rs = 1;
        NewTopic newTopic = new NewTopic(TOPIC_NAME, 1, rs);
        CreateTopicsResult topics = adminClient.createTopics(List.of(newTopic));
        System.out.println("CreateTopicsResult : " + topics);
    }

    /**
     * 获取 topic 列表
     */
    @Test
    public  void topicLists() throws Exception {

        //内置的topic
//        ListTopicsOptions options = new ListTopicsOptions();
//        options.listInternal(true);
        ListTopicsResult listTopicsResult = adminClient.listTopics();
        Set<String> names = listTopicsResult.names().get();
        Collection<TopicListing> topicListings = listTopicsResult.listings().get();
        // 打印names
        names.forEach(System.out::println);
        // 打印topicListings
     //   topicListings.forEach(System.out::println);
    }

    /**
     * topic 删除
     */
    @Test
    public  void delTopics() throws Exception {

        DeleteTopicsResult deleteTopicsResult = adminClient.deleteTopics(Collections.singletonList(TOPIC_NAME));
        deleteTopicsResult.all().get();
    }
    /**
     * 查看topic 描述信息
     */
    @Test
    public  void describeTopics() throws Exception {

        DescribeTopicsResult describeTopicsResult = adminClient.describeTopics(Collections.singletonList(TOPIC_NAME));
        Map<String, TopicDescription> stringTopicDescriptionMap = describeTopicsResult.all().get();
        Set<Map.Entry<String, TopicDescription>> entries = stringTopicDescriptionMap.entrySet();
        entries.forEach((entry) -> System.out.println("name ：" + entry.getKey() + " , desc: " + entry.getValue()));
    }


    /**
     *
     *   查看配置信息
     */
    @Test
    public  void describeConfig() throws Exception {

        ConfigResource configResource = new ConfigResource(ConfigResource.Type.TOPIC, TOPIC_NAME);
        DescribeConfigsResult describeConfigsResult = adminClient.describeConfigs(List.of(configResource));
        Map<ConfigResource, Config> configResourceConfigMap = describeConfigsResult.all().get();
        configResourceConfigMap.forEach((key, value) -> System.out.println("configResource : " + key + " , Config : " + value));
    }



   /**
     * 修改Config信息
     */
   @Test
   public  void alterConfig() throws Exception {

        Map<ConfigResource, Collection<AlterConfigOp>> configMaps = new HashMap<>();
        ConfigResource configResource = new ConfigResource(ConfigResource.Type.TOPIC, TOPIC_NAME);
        AlterConfigOp alterConfigOp =
                new AlterConfigOp(new ConfigEntry("preallocate", "false"), AlterConfigOp.OpType.SET);
        configMaps.put(configResource, List.of(alterConfigOp));

        AlterConfigsResult alterConfigsResult = adminClient.incrementalAlterConfigs(configMaps);
        alterConfigsResult.all().get();
    }

    /**
     * 增加partition数量
     */
    @Test
    public void incrPartitions() throws Exception {

        Map<String, NewPartitions> partitionsMap = new HashMap<>();
        NewPartitions newPartitions = NewPartitions.increaseTo(5);
        partitionsMap.put(TOPIC_NAME, newPartitions);


        CreatePartitionsResult createPartitionsResult = adminClient.createPartitions(partitionsMap);
        createPartitionsResult.all().get();
    }
}
