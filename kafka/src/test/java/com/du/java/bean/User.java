package com.du.java.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 杜瑞龙
 * @date 2023/3/19 0:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    String name;
    Integer age;
}
