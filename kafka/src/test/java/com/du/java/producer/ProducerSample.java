package com.du.java.producer;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import com.alibaba.fastjson2.JSON;
import com.du.java.bean.User;
import org.apache.kafka.clients.producer.*;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProducerSample {

    private final static String TOPIC_NAME = "du-topic";
    static Properties properties = new Properties();

    static {
        LoggerContext loggerContext = (LoggerContext) LoggerFactory.getILoggerFactory();
        Logger root = loggerContext.getLogger("root");
        root.setLevel(Level.INFO);

        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "82.157.62.33:9092");
        properties.put(ProducerConfig.ACKS_CONFIG, "all");
        properties.put(ProducerConfig.RETRIES_CONFIG, "0");
        properties.put(ProducerConfig.BATCH_SIZE_CONFIG, "16384");
        properties.put(ProducerConfig.LINGER_MS_CONFIG, "1");
        properties.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "33554432");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
    }

    static Producer<String, String> producer = new KafkaProducer<>(properties);

    /**
     * 异步阻塞发送
      */
    @Test
    public void syncSend() throws ExecutionException, InterruptedException {
        User du = new User("du", 90);
        ProducerRecord<String, String> stringUserProducerRecord = new ProducerRecord<>("du", JSON.toJSONString(du));
        Future<RecordMetadata> send = producer.send(stringUserProducerRecord);

        //阻塞
        RecordMetadata recordMetadata = send.get();

        System.out.println(recordMetadata.partition());
        System.out.println(recordMetadata.offset());
        producer.close();

    }
    /**
     * 异步发送带回调函数
     */
    @Test
    public void asyncSendCall() throws ExecutionException, InterruptedException {
        User du = new User("du", 955);
        ProducerRecord<String, String> stringUserProducerRecord = new ProducerRecord<>("du", JSON.toJSONString(du));

        producer.send(stringUserProducerRecord,(recordMetadata,ex)->{
            if (null==ex){
                System.out.println("消息发送成功");
            }

        });

        producer.close();
    }

    /*
        Producer异步发送带回调函数和Partition负载均衡
     */
    @Test
    public  void producerSendWithCallbackAndPartition(){

        properties.put(ProducerConfig.PARTITIONER_CLASS_CONFIG,"com.du.config.keyPartition");

        for(int i=0;i<10;i++){
            ProducerRecord<String,String> record =
                    new ProducerRecord<>(TOPIC_NAME,"key-"+i,"value-"+i);
            producer.send(record, new Callback() {
                @Override
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    System.out.println(
                            "partition : "+recordMetadata.partition()+" , offset : "+recordMetadata.offset());
                }
            });
        }
        producer.close();
    }

}
