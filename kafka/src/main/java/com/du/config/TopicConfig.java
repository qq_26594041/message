package com.du.config;

import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.internals.Topic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 杜瑞龙
 * @date 2023/3/24 23:01
 */
@Configuration
public class TopicConfig {

    @Bean
    public NewTopic zhan(){
        return  new NewTopic("dd",4, (short) 1);
    }
}
