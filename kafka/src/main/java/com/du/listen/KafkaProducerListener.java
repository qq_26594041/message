package com.du.listen;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.support.ProducerListener;
import org.springframework.lang.Nullable;

/**
 * @author 杜瑞龙
 * @date 2023/4/1 20:37
 */
@Slf4j
public class KafkaProducerListener  implements ProducerListener<Object,Object> {


    @Override
    public void onSuccess(ProducerRecord producerRecord, RecordMetadata recordMetadata) {
        log.info("KafkaProducerListener 发送成功 "+producerRecord.toString());
    }
    @Override
    public void onError(ProducerRecord<Object, Object> producerRecord,
                        @Nullable RecordMetadata recordMetadata,
                        Exception exception) {
    }

}
