package com.du.producerInterceptor;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerInterceptor;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Map;

/**
 * @author 杜瑞龙
 * @date 2023/4/1 17:05
 */
@Slf4j
public class MyProducerlnterceptor implements ProducerInterceptor<String,Object> {

    /**
     * 消息成功发送次数
     */
    private int successCount = 0 ;
    private int errorCount = 0 ;

    /**
     * Producer确保在消息被序列化以及计算分区前调用该方法。用户可以在该方法中对消息做任何操作
     * <p> 但最好保证不要修改消息所属的topic和分区，否则会影响目标分区的计算。</p>
     *
     */
    @Override
    public ProducerRecord<String, Object> onSend(ProducerRecord<String, Object> record) {



        return record;
    }

    /**
     * 该方法会在消息从RecordAccumulator成功发送到Kafka Broker之后，或者在发送过程中失败时调用。
     * 并且通常都是在producer回调逻辑触发之前。onAcknowledgement运行在producer的IO线程中，
     * 因此不要在该方法中放入很重的逻辑，否则会拖慢producer的消息发送效率
     */
    @Override
    public void onAcknowledgement(RecordMetadata recordMetadata, Exception e) {
        log.info("拦截器执行 onAcknowledgement {}",recordMetadata ,e);

        //统计成功或失败次数
        if(e == null){
            successCount++;
        }else{
            errorCount++;
        }
        log.info("successCount = {} ; errorCount = {}" ,successCount , errorCount );

    }

    /**
     * <p>关闭interceptor，主要用于执行一些资源清理工作
     * interceptor可能被运行在多个线程中，因此在具体实现时用户需要自行确保线程安全
     * </p>
     * <p>另外倘若指定了多个interceptor则producer将按照指定顺序调用它们，并仅仅是捕获每个interceptor可能抛出的异常记录到错误日志中而非在向上传递。
     *  这在使用过程中要特别留意</p>
     */
    @Override
    public void close() {
        log.info("拦截器执行 close");
    }

    /**
     * 获取配置信息和初始化数据时调用
     * @param configs
     */
    @Override
    public void configure(Map<String, ?> configs) {
        log.info("拦截器执行 configure ；map = {}",configs);

    }
}
