package com.du.producerInterceptor;

import org.apache.kafka.clients.consumer.ConsumerInterceptor;
import org.apache.kafka.clients.consumer.internals.ConsumerInterceptors;

import java.util.List;

/**
 * @author 杜瑞龙
 * @date 2023/4/1 20:43
 */
public class MyConsumerInterceptor extends ConsumerInterceptors<String, Object> {
    public MyConsumerInterceptor(List<ConsumerInterceptor<String, Object>> consumerInterceptors) {
        super(consumerInterceptors);
    }
}
