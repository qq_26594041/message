package com.du.controller;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

/**
 * @author 杜瑞龙
 * @date 2023/3/24 21:24
 */
@RestController
@RequestMapping("send/")
@RequiredArgsConstructor
@Slf4j
public class SendMessage {


    final KafkaTemplate<String,Object> kafkaTemplate;

    /**
     *
     * @param messgae 消息内容
     */
    @GetMapping("comple/")
    public  void test(@RequestParam String messgae){


        CompletableFuture<SendResult<String, Object>> completable = kafkaTemplate.send("du", "001", messgae);
        completable.whenCompleteAsync((r,e)->{
           if (null==e){
               RecordMetadata recordMetadata = r.getRecordMetadata();
               ProducerRecord<String, Object> producerRecord = r.getProducerRecord();
               String key = producerRecord.key();
               Object value = producerRecord.value();
               String topic = producerRecord.topic();
               long offset = recordMetadata.offset();
               log.info("生产者[{}],[{}],[{}]",key,offset,value);
           }else {
               e.printStackTrace();
           }
        });

    }

    /**
     * 测试发送失败
     * @param messgae
     */
    @GetMapping("comple/ex")
    public  void testex(@RequestParam String messgae){

        kafkaTemplate.executeInTransaction(operations -> {
            CompletableFuture<SendResult<String, Object>> du = operations.send("du", "001", messgae);
            throw new RuntimeException("edc");
        });
    }
}
