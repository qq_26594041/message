package com.du.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

/**
 * @author 杜瑞龙
 * @date 2023/3/23 23:52
 */
@Component
@Slf4j
public class Myconsumer {

    //KafkaConsumer<String,Object> kafkaConsumer
    @KafkaListener(groupId ="du",topics = "du")
    public  void onMessage(ConsumerRecord<String,Object> consumerRecord,
                           Acknowledgment acknowledgment){
        int partition = consumerRecord.partition();
        String key = consumerRecord.key();
        long offset = consumerRecord.offset();
        Object value = consumerRecord.value();
        String s = String.valueOf(value);
        int i1 = Integer.parseInt(s);
        log.info("消费者[{}],[{}],[{}]",key,offset,value);
        int i=1/i1;
        acknowledgment.acknowledge();
    }

}
