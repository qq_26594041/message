package com.du;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

/**
 * @author du
 */
@SpringBootApplication
@EnableJms
public class ActiveMq {
    public static void main(String[] args) {
        SpringApplication.run(ActiveMq.class, args);
    }
}
