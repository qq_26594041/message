package com.du.listener;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;


/**
 * @author du
 * 监听消息队列
 */
@Component
@RabbitListener(queues = "springMvc_topic_Queue")
@Slf4j
public class BookRabbitListener {

    /**
     * 外部的配置 重试次数
     */
    int retryTime;

    @RabbitHandler
    public void receive(byte[] info,Message message, Channel channel) throws IOException {

        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            //重试次数 次数
            if (retryTime > 5) {
                channel.basicReject(deliveryTag, false);
            } else {
                String s1s = new String(info, StandardCharsets.UTF_8);
                log.info("收到消息[{}]", s1s);
                //处理消息内容
                channel.basicAck(deliveryTag, false);
                //删除数据库记录
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            //延迟 前几次 快速消费，后几次 时间逐渐增加

            //重回队列
            channel.basicNack(deliveryTag, false, true);

        }

    }

    @RabbitHandler
    public void receiveByte(String info) {

        log.info("收到消息[{}]", info);
    }
}