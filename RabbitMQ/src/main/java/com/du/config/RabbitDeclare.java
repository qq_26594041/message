package com.du.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 在使用连接时进行创建
 * @author 杜瑞龙
 * @date 2021/8/22 17:32
 */
@Configuration
public class RabbitDeclare {



    /**
     * 声明交换机
     */

    @Bean
    public Exchange testExchangeTopic() {

        return new TopicExchange("springTopic");
    }

    /**
     * 声明配置队列
     */
    @Bean
    public Queue topicQueue() {
        Map<String, Object> arguments = new HashMap<>(1);
        arguments.put("x-message-ttl",1000);
        arguments.put("x-dead-letter-exchange", "exchange.dlx");
        arguments.put("x-max-length", 3);
        return new Queue("spring_topic_Queue", true,false,false,arguments);
    }

    /**
     * 声明绑定关系
     */
    @Bean
    public Binding topicBinding() {
        return new Binding(
                "spring_topic_Queue",
                Binding.DestinationType.QUEUE,
                "springTopic",
                "spring.#",
                null
        );
    }

    /**
     * 声明交换机
     */

    @Bean
    public Exchange testExchangeTopic1() {
        return new TopicExchange("springMvcTopic");
    }

    /**
     * 声明配置队列
     */
    @Bean
    public Queue topicQueue2() {
        return new Queue("springMvc_topic_Queue");
    }

    /**
     * 声明绑定关系
     */
    @Bean
    public Binding topicBinding1() {
        return new Binding(
                "springMvc_topic_Queue",
                Binding.DestinationType.QUEUE,
                "springMvcTopic",
                "spring.#",
                null
        );
    }
}
