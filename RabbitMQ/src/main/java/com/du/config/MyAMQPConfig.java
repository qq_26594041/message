package com.du.config;

import com.du.service.TransMessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.nio.charset.StandardCharsets;


/**
 * @author du
 */
@Configuration
@RequiredArgsConstructor
@Slf4j
public class MyAMQPConfig {


    final RabbitTemplate rabbitTemplate;
    final TransMessageService transMessageService;


    /**
     * 消息从生产者到exchange会回调confirmCallback这样一个回调函数，来给生产一个应答，是否放入成功，该方法无论消息放入成功或失败都会执行
     */
    @Bean
    public void rabbitTemplateConfirm() {

        rabbitTemplate.setConfirmCallback((correlationData, ack, cause) -> {
            if (ack && null !=correlationData) {
                log.info("correlationId: {}，ack：{}，cause：{}"
                        , correlationData.getId(), ack, cause);
                String messageId =correlationData.getId();
                log.info("消息已经正确投递到交换机，id:{}",messageId);
                transMessageService.messageSendSuccess(messageId);
            }else{
                log.error("消息投递至交换机失败，correlationData:{}",correlationData);
            }
        });


        /*
         * 消息从exchange到queue，投递失败则会返回一个returnCallback函数 注意是失败时才会回调
         */
        rabbitTemplate.setReturnsCallback((returnedMessage) -> {
            Message message = returnedMessage.getMessage();
            int replyCode = returnedMessage.getReplyCode();
            String replyText = returnedMessage.getReplyText();
            String exchange = returnedMessage.getExchange();
            String routingKey = returnedMessage.getRoutingKey();

            log.error("消息无法路由！message:{},replyCode:{}, replyText:{}, exchange:{}, routingKey:{}",
                         message,replyCode,replyText,exchange,routingKey);

            transMessageService.messageSendReturn(
                    message.getMessageProperties().getMessageId(),
                    returnedMessage.getExchange(),
                    returnedMessage.getRoutingKey(),
                    new String(message.getBody(), StandardCharsets.UTF_8)
                    );
        });

    }
}
