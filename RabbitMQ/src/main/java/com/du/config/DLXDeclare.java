package com.du.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 杜瑞龙
 * @date 2023/4/7 19:42
 */
@Configuration
public class DLXDeclare {

    @Bean
    public Exchange testExchangeTopicDlx() {

        return new TopicExchange("exchange.dlx");
    }

    /**
     * 声明配置队列
     */
    @Bean
    public Queue topicQueueDlx() {
        return new Queue("queue.dlx");
    }

    /**
     * 声明绑定关系
     */
    @Bean
    public Binding topicBindingDlx() {
        return new Binding(
                "queue.dlx",
                Binding.DestinationType.QUEUE,
                "exchange.dlx",
                "#",
                null
        );
    }

}
