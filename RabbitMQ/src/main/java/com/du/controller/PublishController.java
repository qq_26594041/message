package com.du.controller;

import com.alibaba.fastjson2.JSON;
import com.du.bean.Book;
import com.du.sender.TransMessageSender;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitMessagingTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

/**
 * @author 杜瑞龙
 * @date 2023/4/6 15:21
 */
@RestController
@Slf4j
@RequestMapping("/send")
@RequiredArgsConstructor
public class PublishController {

    final TransMessageSender transMessageSender;
    final RabbitTemplate rabbitTemplate;

    /**
     *  发送消息
     */
    @GetMapping("/book")
    public void sendBook(){
        CorrelationData correlationData = new CorrelationData(UUID.randomUUID().toString());

        transMessageSender.send("springMvcTopic","spring.ttl",new Book("西游记", "吴承恩"));

    }

    /**
     * 原生API的使用
     */
    public void javaApi(){
        rabbitTemplate.execute(channel -> {

            return null;
        });
    }

}
