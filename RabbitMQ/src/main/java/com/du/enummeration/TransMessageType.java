package com.du.enummeration;

/**
 * @author 杜瑞龙
 * @date 2023/4/8 14:49
 */
public enum TransMessageType {
    SEND,
    RECEIVE,
    DEAD;

}
