package com.du;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author du
 */
@EnableRabbit
@EnableScheduling
@SpringBootApplication
public class RabbitMQ {

    public static void main(String[] args) {
        SpringApplication.run(RabbitMQ.class, args);
    }

}
