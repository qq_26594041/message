package com.du.task;

import com.du.bean.TransMessagePo;
import com.du.service.TransMessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author 杜瑞龙
 * @date 2023/4/8 15:02
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class ResendTask {

    final RabbitTemplate rabbitTemplate;
    final TransMessageService transMessageService;

    /**
     * 外部的配置 重试次数
     */
    int retryTime;

    /**
     * 监听 失败的消息 进行重新发送
     */
    @Scheduled(fixedDelayString = "5000")
    public void resendMessage() {

        //重试 大于 指定次数
        if (retryTime > 5) {
            log.info("重试失败");

            //更新次数
        } else {

            log.info("resendMessage()invoked.");
            List<TransMessagePo> transMessagePos = transMessageService.listReadyMessages();

            //延迟 前几次 快速消费，后几次 时间逐渐增加

            //发送消息

            //更新记录

        }
    }
}
