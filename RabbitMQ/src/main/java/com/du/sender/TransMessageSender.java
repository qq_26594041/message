package com.du.sender;

import com.alibaba.fastjson2.JSON;
import com.du.bean.TransMessagePo;
import com.du.service.TransMessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;


/**
 * @author 杜瑞龙
 * @date 2023/4/8 17:17
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class TransMessageSender {

    final RabbitTemplate rabbitTemplate;
    final TransMessageService transMessageService;

    public void send(String exchange, String routingKey, Object payload) {
        log.info("send():exchange:{} routingKey:{}  payload:{}", exchange, routingKey, payload);
        try {
            String payloadString = JSON.toJSONString(payload);
            TransMessagePo transMessagePo = transMessageService.messageSendReady(exchange, routingKey, payloadString);

            MessagePostProcessor messagePostProcessor = new MessagePostProcessor() {
                @Override
                public Message postProcessMessage(Message message) throws AmqpException {
                    message.getMessageProperties().setContentEncoding("UTF-8");
                    message.getMessageProperties().setContentType(MessageProperties.CONTENT_TYPE_JSON);
                    message.getMessageProperties().setMessageId(transMessagePo.getId());
                    return message;
                }
            };
            rabbitTemplate.convertAndSend(exchange, routingKey,
                    payloadString,
                    messagePostProcessor,
                    new CorrelationData(transMessagePo.getId()));

            log.info("message sent,ID:{}", transMessagePo.getId());
        } catch (Exception e) {
            log.error(e.getMessage(), e);

        }
    }

    public void send(String exchange, String routingKey, Object payload, String ttl) {
        log.info("send():exchange:{} routingKey:{}  payload:{}", exchange, routingKey, payload);
        try {
            String payloadString = JSON.toJSONString(payload);
            TransMessagePo transMessagePo = transMessageService.messageSendReady(exchange, routingKey, payloadString);

            MessagePostProcessor messagePostProcessor = new MessagePostProcessor() {
                @Override
                public Message postProcessMessage(Message message) throws AmqpException {
                    message.getMessageProperties().setContentEncoding("UTF-8");
                    message.getMessageProperties().setExpiration(ttl);
                    message.getMessageProperties().setContentType(MessageProperties.CONTENT_TYPE_JSON);
                    message.getMessageProperties().setMessageId(transMessagePo.getId());
                    return message;
                }
            };
            rabbitTemplate.convertAndSend(exchange, routingKey,
                    payloadString,
                    messagePostProcessor,
                    new CorrelationData(transMessagePo.getId()));

            log.info("message sent,ID:{}", transMessagePo.getId());
        } catch (Exception e) {
            log.error(e.getMessage(), e);

        }
    }
}
