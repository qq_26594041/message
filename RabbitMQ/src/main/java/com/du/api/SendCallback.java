package com.du.api;

/**
 * 	$SendCallback 回调函数处理
 * @author Alienware
 *
 */
public interface SendCallback {

	void onSuccess();
	
	void onFailure();
	
}
