package com.du.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author du
 */
@Data
@AllArgsConstructor
public class Book {
    private String bookName;
    private String author;
}
