package com.du.bean;

import com.du.enummeration.TransMessageType;
import lombok.Data;

import java.util.Date;

/**
 * @author 杜瑞龙
 * @date 2023/4/8 14:50
 */
@Data
public class TransMessagePo {

    private String id;
    private String  service;
    private TransMessageType type;
    private String exchange;
    private String routingKey;
    private String queue;
    private int sequence;
    private String payload;
    private Date date;

}
