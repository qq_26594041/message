package com.du.service.impl;

import com.du.bean.TransMessagePo;
import com.du.dao.TransMessageDao;
import com.du.enummeration.TransMessageType;
import com.du.service.TransMessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author 杜瑞龙
 * @date 2023/4/8 17:02
 */
@Service
@RequiredArgsConstructor
public class TransMessageServiceImpl implements TransMessageService {


    final TransMessageDao transMessageDao;


    @Override
    public TransMessagePo messageSendReady(String exchange, String routingKey, String body) {

        TransMessagePo transMessagePo= new TransMessagePo();
        transMessagePo.setId(UUID.randomUUID().toString());
        transMessagePo.setService("dd");
        transMessagePo.setExchange(exchange);
        transMessagePo.setRoutingKey(routingKey);
        transMessagePo.setPayload(body);
        transMessagePo.setDate(new Date());
        transMessagePo.setSequence(0);
        transMessagePo.setType(TransMessageType.SEND);
        return transMessagePo;
    }

    @Override
    public void messageSendSuccess(String id) {

        transMessageDao.delete(id,"serviceName");

    }

    @Override
    public TransMessagePo messageSendReturn(String id, String exchange, String routingKey, String body) {
        return messageSendReady(exchange,routingKey,body);
    }

    @Override
    public List<TransMessagePo> listReadyMessages() {
        return transMessageDao.selectByTypeAndService(
                TransMessageType.SEND.toString(),"serviceName");
    }

    @Override
    public void messageResend(String id) {
        TransMessagePo transMessagePo =transMessageDao.selectByIdAndService(id,"serviceName");
        //注意分布式锁 不能 加多次
        transMessagePo.setSequence(transMessagePo.getSequence()+1);
        transMessageDao.update(transMessagePo);
    }

    @Override
    public void messageDead(String id) {
        TransMessagePo transMessagePo =transMessageDao.selectByIdAndService(id,"serviceName");
        transMessagePo.setType(TransMessageType.DEAD);
        transMessageDao.update(transMessagePo);
    }
}
