package com.du.service;

import com.du.bean.TransMessagePo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 杜瑞龙
 * @date 2023/4/8 16:54
 */
@Service
public interface TransMessageService {

    /**
     * 发送前 暂存 消息
     */
    TransMessagePo messageSendReady(String exchange, String routingKey, String body);

    /**
     *设置消息的发送成功
     * @param id
     */
    void messageSendSuccess(String id);

    /**
     *设置消息返回
     */
    TransMessagePo messageSendReturn(
            String id,String exchange,String routingKey,String body);

    /**
     * 查询 未发送成功消息
     */
    List<TransMessagePo> listReadyMessages ();

    /**
     * 记录消息 发送的次数
     */
    void messageResend(String id);

    /**
     * 消息重发多次 放弃
     */
    void messageDead(String id);

}
