package com.du.abbit;

import com.du.bean.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

/**
 * 死信队列
 */
public class DLXMessage {
    @Test
    public void createInfo() {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.115");

        try (Connection connection = connectionFactory.newConnection(); Channel channel = connection.createChannel()) {
            channel.exchangeDeclare("exchange_testDirect", BuiltinExchangeType.DIRECT, true);
            HashMap<String, Object> args = new HashMap<>();
            args.put("x-message-ttl", 150000);
            args.put("x-dead-letter-exchange", "exchange.dlx");
            args.put("x-max-length", 3);

            channel.queueDeclare("queue.direct5", true, false, false, args);
            channel.queueBind("queue.direct5", "exchange_testDirect", "key_direct5");
            /**
             * 声明死信的交换机
             */
            channel.exchangeDeclare("exchange.dlx", BuiltinExchangeType.TOPIC, true, false, null);
            /**
             * 声明死信队列
             */
            channel.queueDeclare("queue.dlx", true, false, false, null);
            channel.queueBind("queue.dlx", "exchange.dlx", "#");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 消息过期  死信转移测试
     */
    @Test
    public void sendMessageDirectDLXTTL() {
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("82.168.0.115");

        try (Connection connection = connectionFactory.newConnection(); Channel channel = connection.createChannel()) {
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            channel.basicPublish("exchange_testDirect", "key_direct5", null, s.getBytes(StandardCharsets.UTF_8));
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 模拟消息拒收后，死信转移
     */
    @Test
    public void sendMessageDirectDLXReject() {
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.115");

        try (Connection connection = connectionFactory.newConnection(); Channel channel = connection.createChannel()) {
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            channel.basicPublish("exchange_testDirect", "key_direct5", null, s.getBytes(StandardCharsets.UTF_8));
            channel.basicConsume("queue.direct5", false, (tag, message) -> {
                long deliveryTag = message.getEnvelope().getDeliveryTag();
                System.out.println(deliveryTag);
                channel.basicNack(deliveryTag, false, false);
                String s1s = new String(message.getBody(), StandardCharsets.UTF_8);
            }, System.out::println);
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }
}
