package com.du.abbit;


import com.du.bean.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;


public class RabbitTTL {

    /**
     * 消息的TTL：过期时间
     */
    @Test
    public void sendMessageDirectTTL(){
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");

        try( Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            AMQP.BasicProperties props = new AMQP.BasicProperties().builder().expiration("150000").build();
            channel.basicPublish("exchange_testDirect","key_direct5",props,s.getBytes(StandardCharsets.UTF_8));
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * <P>1：只能在声明中使用</P>
     * 2：更改已经存在的队列，需要删除以前存在的重新声明
     */
    @Test
    public void  createInfo(){
        //使用默认的用户名
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");

        try( Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            channel.exchangeDeclare("exchange_testDirect", BuiltinExchangeType.DIRECT,true);
             HashMap<String, Object> args = new HashMap<>();
             args.put("x-message-ttl",15000);
            channel.queueDeclare("queue.direct4",true,false,false,args);
            channel.queueBind("queue.direct4","exchange_testDirect","key_direct3");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
