package com.du.message.sure;

import com.du.bean.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * @author 杜瑞龙
 * @date 2021/8/14 11:25
 */

public class ConsumerMessageSure {



    /**
     * 手动 不进行签收
     */
    @Test
    public void ConsumerMessageSureNoS() {
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.115");

        try (Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            channel.basicPublish("exchange_testDirect", "key_direct1", null, s.getBytes(StandardCharsets.UTF_8));

            channel.basicConsume("queue.direct1", false, (tag, message) -> {
                String s1s = new String(message.getBody(), StandardCharsets.UTF_8);
            }, System.out::println);
            Thread.sleep(100000);
        } catch (TimeoutException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    /**
     * 消费端 手动签收
     */
    @Test
    public void ConsumerMessageSureS() {
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("82.154.62.33");

        try (Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            channel.basicPublish("exchange_testDirect", "key_direct1", null, s.getBytes(StandardCharsets.UTF_8));

            channel.basicConsume("queue.direct1", false, (tag, message) -> {
                long deliveryTag = message.getEnvelope().getDeliveryTag();
                System.out.println(deliveryTag);
                //每五条签收一次
                if (deliveryTag % 5 == 0) {
                    channel.basicAck(deliveryTag, true);
                }
                String s1s = new String(message.getBody(), StandardCharsets.UTF_8);
            }, System.out::println);
            Thread.sleep(1000);
        } catch (TimeoutException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 重回队列
     */
    @Test
    public void ConsumerMessageSureReturn() {
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.115");

        try (Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            channel.basicPublish("exchange_testDirect", "key_direct1", null, s.getBytes(StandardCharsets.UTF_8));

            channel.basicConsume("queue.direct1", false, (tag, message) -> {
                long deliveryTag = message.getEnvelope().getDeliveryTag();
                System.out.println(deliveryTag);
                channel.basicNack(deliveryTag, false, true);

                String s1s = new String(message.getBody(), StandardCharsets.UTF_8);
            }, System.out::println);
            Thread.sleep(100000);
        } catch (TimeoutException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 限流

     */
    @Test
    public void ConsumerMessageLimitStream() {
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.115");

        try (Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            for (int i = 0; i < 50; i++) {
                channel.basicPublish("exchange_testDirect", "key_direct1", null, s.getBytes(StandardCharsets.UTF_8));

            }
            channel.basicQos(2);
            channel.basicConsume("queue.direct1", false, (tag, message) -> {
                long deliveryTag = message.getEnvelope().getDeliveryTag();
                System.out.println(deliveryTag);
                try {
                    channel.basicAck(deliveryTag, false);
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                String s1s = new String(message.getBody(), StandardCharsets.UTF_8);
            }, System.out::println);
            Thread.sleep(300000000);
        } catch (TimeoutException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }





}

