package com.du.message.sure;

import com.du.bean.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmListener;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 *  发送端 消息
 */
public class ReliabilitySendMessage {



    /**
     *  <p>1.配置channel,开启确认模式：channel.confirmSelect()</p>
     *  <P>2.每发送一条消息，调用channel.waitForConfirms()，等待确认</P>
     */
    @Test
    public void sendMessageDirectConfirmSingle(){
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");
        try(Connection connection = connectionFactory.newConnection();
            Channel channel = connection.createChannel()
        ) {
            channel.confirmSelect();
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            channel.basicPublish("xchange.order.restaurant","key.order",null,s.getBytes(StandardCharsets.UTF_8));
            boolean b = channel.waitForConfirms();
            System.out.println(b);
        } catch (TimeoutException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
    /**
     * <p>1.配置channel,开启确认模式：channel.confirmSelect()</p>
     * 2.每发送一条消息，调用channel.waitForConfirms()，等待确认, 无法确认是否那条
     */
    @Test
    public void sendMessageDirectConfirmMany(){
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.114");
        try(Connection connection = connectionFactory.newConnection();
            Channel channel = connection.createChannel()
        ) {
            channel.confirmSelect();
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            for (int i = 0; i <20 ; i++) {
                channel.basicPublish("xchange.order.restaurant","key.order",null,s.getBytes(StandardCharsets.UTF_8));
            }

            //无法确定具体的那一条
            boolean b = channel.waitForConfirms();
            System.out.println(b);
        } catch (TimeoutException | IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }
    /**
     * 1.配置channel,开启确认模式：channel.confirmSelect()
     * 2.在channel上添加监听：addConfirmListener，发送消息后，会调用此方法，通知是否发送成功
     * 3.异步确认可能是单条，也可能是多条，取决于MQ
     */
    @Test
    public void asyncSendMessage(){
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.114");
        try(Connection connection = connectionFactory.newConnection();
            Channel channel = connection.createChannel()
        ) {
            channel.confirmSelect();
            ConfirmListener confirmListener=   new ConfirmListener() {
                @Override
                public void handleAck(long l, boolean b) throws IOException {

                    System.out.println("success");
                    System.out.println(l+"------"+b);
                }

                //失败
                @Override
                public void handleNack(long l, boolean b) throws IOException {

                    System.out.println("fail");
                    System.out.println(l+"------"+b);
                }
            };
            channel.addConfirmListener(confirmListener);
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            for (int i = 0; i <20 ; i++) {
                channel.basicPublish("xchange.order.restaurant","key.order",null,s.getBytes(StandardCharsets.UTF_8));
                System.out.println(i+"dddd");
            }

            Thread.sleep(100000);

        } catch (TimeoutException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }




}
