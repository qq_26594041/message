package com.du.declare;

import jakarta.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueInformation;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author 杜瑞龙
 * @date 2023/7/20 21:59
 */
@SpringBootTest
public class RabbitAdminTest {

    @Resource
    RabbitAdmin rabbitAdmin;
    @Test
    public void test() {

        QueueInformation queueInfo = rabbitAdmin.getQueueInfo("77");

        rabbitAdmin.declareExchange(new DirectExchange("spring_DirectExchage"));
        rabbitAdmin.declareQueue(new Queue("spring_DirectQueue"));
        rabbitAdmin.declareBinding(
                new Binding(
                        "spring_DirectQueue",
                        Binding.DestinationType.QUEUE,
                        "spring_DirectExchage",
                        "spring_direct",
                        null));
    }
}
