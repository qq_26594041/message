package com.du.modle;

import com.du.bean.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * @author 杜瑞龙
 * @date 2021/8/10 14:58
 */
public class Topic {
    @Test
    public void consumeDirect() {
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");

        try(Connection connection = connectionFactory.newConnection();
            Channel channel = connection.createChannel()
        ) {
            channel.basicConsume("Topic_java", true, (tag,message)->{
                String s = new String(message.getBody(), StandardCharsets.UTF_8);
                Book book = objectMapper.readValue(s, Book.class);
            }, System.out::println);
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void sendMessageDirect(){
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");

        try( Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            channel.basicPublish("exchange_testTopic","L.i.e",null,s.getBytes(StandardCharsets.UTF_8));
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void  createInfo(){
        //使用默认的用户名
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");

        try( Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            channel.exchangeDeclare("exchange_testTopic", BuiltinExchangeType.TOPIC,true);
            channel.queueDeclare("Topic_java", true,false,false,null);
            channel.queueDeclare("Topic_python", true,false,false,null);
            channel.queueDeclare("Topic_docker", true,false,false,null);
            channel.queueDeclare("Topic_tomcat", true,false,false,null);

            channel.queueBind("Topic_java","exchange_testTopic","L.#");
            channel.queueBind("Topic_python","exchange_testTopic","L.#");
            channel.queueBind("Topic_docker","exchange_testTopic","F.*");
            channel.queueBind("Topic_tomcat","exchange_testTopic","F.*");

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
