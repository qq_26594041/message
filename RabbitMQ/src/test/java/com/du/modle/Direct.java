package com.du.modle;

import com.du.bean.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * @author 杜瑞龙
 * @date 2021/8/7 16:59
 */
public class Direct {

    @Test
    public void consumeDirect() {
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");

        try( Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            channel.basicConsume("queue.order", true, (tag,message)->{
                String s = new String(message.getBody(), StandardCharsets.UTF_8);
                Book book = objectMapper.readValue(s, Book.class);
            }, System.out::println);
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void sendMessageDirect(){
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");

        try( Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            channel.basicPublish("spring_DirectExchage","spring_direct",null,s.getBytes(StandardCharsets.UTF_8));
            channel.basicPublish("exchange_testDirect","key_direct2",null,s.getBytes(StandardCharsets.UTF_8));
            channel.basicPublish("exchange_testDirect","key_direct3",null,s.getBytes(StandardCharsets.UTF_8));
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void  createInfo(){
        //使用默认的用户名
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");

        try( Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            channel.exchangeDeclare("exchange_testDirect", BuiltinExchangeType.DIRECT,true);
            channel.queueDeclare("queue.direct1",true,false,false,null);
            channel.queueDeclare("queue.direct2",true,false,false,null);
            channel.queueDeclare("queue.direct3",true,false,false,null);

            channel.queueDeclare("queue.direct4",true,false,false,null);
            channel.queueBind("queue.direct1","exchange_testDirect","key_direct1");
            channel.queueBind("queue.direct2","exchange_testDirect","key_direct2");
            channel.queueBind("queue.direct3","exchange_testDirect","key_direct3");
            channel.queueBind("queue.direct4","exchange_testDirect","key_direct3");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
