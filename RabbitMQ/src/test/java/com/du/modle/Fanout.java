package com.du.modle;

import com.du.bean.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * @author 杜瑞龙
 * @date 2021/8/10 14:58
 */
public class Fanout {
    @Test
    public void consumeDirect() {
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");

        try(Connection connection = connectionFactory.newConnection();
            Channel channel = connection.createChannel()
        ) {
            channel.basicConsume("fanout_01", true, (tag,message)->{
                String s = new String(message.getBody(), StandardCharsets.UTF_8);
                Book book = objectMapper.readValue(s, Book.class);
            }, System.out::println);
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void sendMessageDirect(){
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");

        try( Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            channel.basicPublish("exchange_testFanout","key_fanout",null,s.getBytes(StandardCharsets.UTF_8));
        } catch (TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void  createInfo(){
        //使用默认的用户名
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");

        try( Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            channel.exchangeDeclare("exchange_testFanout", BuiltinExchangeType.FANOUT,true);
            channel.queueDeclare("fanout_01", true,false,false,null);
            channel.queueDeclare("fanout_02", true,false,false,null);
            channel.queueDeclare("fanout_03", true,false,false,null);
            channel.queueDeclare("fanout_04", true,false,false,null);
            channel.queueBind("fanout_01","exchange_testFanout","key_fanout");
            channel.queueBind("fanout_02","exchange_testFanout","key_fanout");
            channel.queueBind("fanout_03","exchange_testFanout","key_fanout");
            channel.queueBind("fanout_04","exchange_testFanout","key_fanout");

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
