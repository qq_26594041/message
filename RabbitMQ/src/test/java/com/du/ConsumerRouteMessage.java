package com.du;

import com.du.bean.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

/**
 * @author 杜瑞龙
 * @date 2021/8/13 21:48
 */
@Slf4j
public class ConsumerRouteMessage {

    /**
     *消息发送后，中间件会对消息进行路由
     * 若没有发现目标队列，中间件会通知发送方
     * Return Listener会被调用
     *            channel.addReturnListener(aReturn -> {
     *                 log.error("code：{}，text：{}", aReturn.getReplyCode(),aReturn.getReplyText());
     *             });
     * Mandatory
     * 为 false，RabbitMQ将直接丢弃无法路由的消息
     * 为true，Rabbit RabbitMQ才会处理无法路由的消息
     *   //第二个参数
     *   channel.basicPublish("xchange.order.restaurant", "key.order", true, null, s.getBytes(StandardCharsets.UTF_8));
     *
     */
    @Test
    public void consumeRouteSure() {
        ObjectMapper objectMapper = new ObjectMapper();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.0.113");
        //try块退出的时候，会自动关闭
        try (Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()
        ) {
            /*没有发现目标队列时调用
             */
            channel.addReturnListener(aReturn -> {
                log.error("code：{}，text：{}", aReturn.getReplyCode(),aReturn.getReplyText());
            });
            Book book = new Book("cj", "mysql");
            String s = objectMapper.writeValueAsString(book);
            channel.basicPublish("xchange.order.restaurant", "key.order", true, null, s.getBytes(StandardCharsets.UTF_8));
            Thread.sleep(10000);
        } catch (TimeoutException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
