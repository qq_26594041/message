package com.du.demo;

import jakarta.jms.*;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;


/**
 * @author du
 */
public class MqProducer {

    public static final String USERNAME = ActiveMQConnection.DEFAULT_USER;
    public static final String PASSWORD = ActiveMQConnection.DEFAULT_PASSWORD;
    public static final String BROKEURL = "tcp://192.168.0.114:61617";
    public static final int SENDNUM = 10;

    /**
     * 发送消息
     */
    public static void sendMsg() {
        ConnectionFactory connectionFactory;
        Connection connection = null;
        Session session;
        Destination destination;
        MessageProducer messageProducer;

        connectionFactory = new ActiveMQConnectionFactory(USERNAME, PASSWORD, BROKEURL);
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            /**
             * 参数1：是否启用事务
             * 参数2: 签收模式 一般设为自动签收
             */
            session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
            destination = session.createQueue("HelloWorld");
            messageProducer = session.createProducer(destination);
            for (int i=0;i<SENDNUM;i++){
                TextMessage textMessage = session.createTextMessage("ActiveMQ发送消息" + i);
                System.out.println("发送消息：Activemq 发送消息"+i);
                messageProducer.send(textMessage);
            }
            session.commit();
        } catch (JMSException e) {
            e.printStackTrace();
        }finally {
            if (connection!=null){
                try {
                    connection.close();
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
