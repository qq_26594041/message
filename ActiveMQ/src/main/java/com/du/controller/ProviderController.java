package com.du.controller;

import jakarta.jms.Queue;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 杜瑞龙
 * @date 2021/4/27 23:01
 */
@RestController
@RequestMapping("activeMq")
@RequiredArgsConstructor
public class ProviderController {


   final Queue queue;
   final JmsMessagingTemplate jmsMessagingTemplate;

    /**
     * 气象元数据新增的 activeMq 队列
     *
     * @param info 队列消息体
     */
    @GetMapping("/send/element/insert")
    public void sendElement(@RequestParam String info) {
        //方法一：添加消息到消息队列
        jmsMessagingTemplate.convertAndSend(queue, info);
        //方法二：这种方式不需要手动创建queue，系统会自行创建名为test的队列
        //jmsMessagingTemplate.convertAndSend("test", name);
    }
}
