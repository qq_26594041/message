package com.du.config;

import lombok.RequiredArgsConstructor;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;
import jakarta.jms.Topic;
/**
 * @author 杜瑞龙
 * @date 2021/4/28 18:54
 */
@Component
@RequiredArgsConstructor
public class TopicProducer {

   final private JmsMessagingTemplate jmsMessagingTemplate;

   final private Topic topic;

    public void sendTopic(String msg) {
        this.jmsMessagingTemplate.convertAndSend(this.topic, msg);
    }

}
