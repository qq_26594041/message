package com.du.config;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * @author 杜瑞龙
 * @date 2021/4/28 21:29
 */
@Component
public class TopicConsumer {
    @JmsListener(destination = "elementUpdate")
    public void receiveTopic(String text) {
        System.out.println(text);
    }
}
