package com.du.config;

import lombok.RequiredArgsConstructor;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

/**
 * @author 杜瑞龙
 * @date 2021/4/28 17:14
 */
@Component
@RequiredArgsConstructor
public class ConsumerService {

    @JmsListener(destination = "elementInsert")
    @SendTo("SQueue")
    public String handleMessage(String name) {
        System.out.println("成功接受Name" + name);
        return "成功接受Name" + name;
    }
}
