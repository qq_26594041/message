package com.du.config;

import jakarta.jms.Queue;
import jakarta.jms.Topic;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 杜瑞龙
 * @date 2021/4/27 22:35
 */

@Configuration
public class ActiveMessageConfig {

    /**
     * 气象元数据 新增的消息队列
     * @return s
     */
    @Bean
    public Queue elementInsert(){
        return  new ActiveMQQueue("elementInsert");
    }

    @Bean
    public Topic topic() {
        return new ActiveMQTopic("elementUpdate");
    }
}
