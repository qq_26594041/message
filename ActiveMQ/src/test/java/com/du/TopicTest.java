package com.du;

import com.du.config.ActiveMessageConfig;
import com.du.config.TopicProducer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author 杜瑞龙
 * @date 2021/4/28 18:26
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class TopicTest {
    @Autowired
    private TopicProducer topicProducer;

    @Test
    public void contextLoads() {
        for (int i = 0; i < 100; i++) {
            this.topicProducer.sendTopic("Topic!!!" + i);
        }
    }
}
