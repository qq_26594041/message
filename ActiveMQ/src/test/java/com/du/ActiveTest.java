package com.du;

import com.du.demo.MqProducer;
import org.junit.jupiter.api.Test;

/**
 * @author 杜瑞龙
 * @date 2021/4/26 22:51
 */
public class ActiveTest {

    /**
     * 发送消息的测试
     */
    @Test
    public void send(){

      MqProducer.sendMsg();

    }
}
